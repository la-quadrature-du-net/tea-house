# Tea House

Non code related project, mostly use to share info and keep a tab on hwo we
build the tea house and where.

## Contact

A lot of discussion about Tea Houses happens on the ohm@laquadrature.net mailing
list. We try to keep the conversation in English and you can join it over
[here](https://lists.laquadrature.net/cgi-bin/mailman/listinfo/ohm).

This gitlab instance is, however, being spammed a lot those days, so you need to
ask for an account first. Send us a mail on the list for this.

## How this repository works

We're creating a branch by event where we will have a tea house. The master branch
will hold checklists and general notes, to help bootstraping new iterations of
the tea house.

And then, using Milestones (and Issues) we will raise and try to solve issues as
they arrive.

Have fun, drink tea.